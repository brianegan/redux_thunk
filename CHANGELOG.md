# Changelog 

## 0.1.0

  * Initial version, includes a `thunkMiddleware`, which intercepts and calls functions that are dispatched to the Store.

